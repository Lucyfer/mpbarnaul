<?php $r = Admin_Model::get_admin_login(Admin_Model::is_admin_auth());?>
<table style='width:100%'>
	<tr>
	  <?php  if (Admin_Model::check_write_news($r->admin_login)):?>
		<td><a href='/admin/news'>Новости</a></td>
	  <?php endif ?>
	  <?php if (Admin_Model::check_write_convocations($r->admin_login)):?>	
		<td><a href='/admin/convocation'>Созывы</a></td>
	  <?php endif ?>
	  <?php if (Admin_Model::check_write_workgroup($r->admin_login)):?>	
		<td><a href='/admin/group'>Группы</a></td>
	  <?php endif ?>
	  <?php if (Admin_Model::check_edit_deput($r->admin_login)):?>	
		<td><a href='/admin/deputy'>Депутаты</a></td>
	  <?php endif ?>
	  <?php if (Admin_Model::check_write_archiv($r->admin_login)):?>	
		<td><a href='/admin/docs'>Документы</a></td>
	  <?php endif ?>
	  <?php if (Admin_Model::check_write_info($r->admin_login)):?>	
		<td><a href='/admin/about'>Информация о парламенте</a></td>
	  <?php endif ?>	
	</tr>
</table>