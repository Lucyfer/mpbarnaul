<?php echo View::factory('admin/topmenu') ?>

<?php
if ($ID): 
	$d = Arch_Model::get_doc_info($ID);
	if ($d): ?>
	<form class='adm_form' action='/admin/save/doc_upd/<?=$ID?>' method='post'>
		<br>
		<span><b>Заголовок:</b></span> <input title='заголовок документа (теги вырезаются)' type='text' name='doc[]' value='<?=$d->doc_title?>' size='100' />
		<br>
		<span><b>Содержание:</b></span>
		<br>
		<textarea id='area1' cols='70' rows='15' name='doc[]'><?=$d->doc_text?></textarea>
		<br>
		<input type='submit' value='Сохранить изменения' />
	</form>
	
	<script type='text/javascript' src='/js/nicEdit.js'></script>
	<script type="text/javascript"><!--
		bkLib.onDomLoaded(function() { new nicEditor({fullPanel : true}).panelInstance('area1'); });
		//-->
	</script>

	<?php else: ?>
	<span>Документ не найден</span>
	<?php endif ?>
	
<?php else: ?>
	<p class='adm_form'>
		<a href='/admin/new_doc'>Создать новый документ</a>
	</p>
	<?php
	$doc = Arch_Model::get_all_docs();
	if ($doc): ?>
	<form class='adm_form' action='/admin/save/del_doc' method='post'>
	<table class='adm_form'>
	<?php foreach ($doc as $d): ?>
		<tr>
			<td><input type='checkbox' name='doc[]' value='<?=$d->doc_id?>' /></td>
			<td><a href='/admin/docs/<?=$d->doc_id?>'><?=$d->doc_title?></a></td>
		</tr>
	<?php endforeach ?>
	</table>
	<input type='submit' value='Удалить' title='Удалить отмеченные' />
	</form>
	
	<?php else: ?>
	<span>Нет документов</span>
	<?php endif ?>
	
<?php endif ?>