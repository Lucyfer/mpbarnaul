<?php echo View::factory('admin/topmenu') ?>

<?php if ($ID): ?>

	<div class='adm_form'>
	
	<span>Состав группы:</span>
	<form class='adm_form' action='/admin/save/del_from_gr/<?=$ID?>' method='post'>
	<table>
		<?php
			$res = Conv_Model::get_staff_group($ID);
			foreach ($res as $s): ?>
		<tr>
			<td><input type='checkbox' name='dep_del[]' value='<?=$s->dep_id?>' /></td>
			<td><a title='перейти к редактированию данных' href='/admin/deputy/<?=$s->dep_id?>'><?=$s->fio?></a></td>
		</tr>
		<?php
			endforeach;?>
	</table>
	<?php if (empty($res)):?>
	<span><i>состав еще не определен</i></span>
	<?php else: ?>
	<input type='submit' title='исключить из состава группы' value='исключить отмеченных' />
	<?php endif ?>
	</form>
	<hr>

<?php //echo '<p>todo форма, вывод списка депутатов(которых нет в группе), кнопка добавить</p>' ?>
	<form class='adm_form' action='/admin/save/add_to_gr/<?=$ID?>' method='post'>
		<?php
		$res = Conv_Model::deputies_not_in_group($ID);
		if ($res): ?>
		<span>Добавить отмеченных депутатов в рабочую группу:</span>
		<table>
			<?php
			foreach ($res as $d):?>
			<tr>
				<td><input type='checkbox' name='dep_add[]' value='<?=$d->dep_id?>' /></td>
				<td><?=$d->fio?></td>
			</tr>
			<?php endforeach ?>
		</table>
		<input type='submit' title='добавить в состав группы' value='добавить в состав' />
		<?php else: ?>
		<span><i>Все депутаты добавлены в рабочую группу</i></span>
		<?php endif ?>
	</form>

	</div>

<?php else: ?>

<br>
<form class='adm_form' action='/admin/save/group_add' method='post'>
	<span>новая группа:</span> <input type='text' name='group_add' /> <input type='submit' value='добавить' />
</form>

<form class='adm_form' action='/admin/save/group_del' method='post'>
<span>Список рабочих групп:</span>
<table class='adm_form'>
<?php foreach (Workgroup_Model::get_groups() as $c):?>
	<tr>
		<td><input type='checkbox' name='group_del[]' value='<?=$c->group_id?>' /></td>
		<td><a title='редактировать состав' href='/admin/group/<?=$c->group_id?>'><?=$c->group_title?></a></td>
	</tr>
<?php endforeach ?>
</table>
<input type='submit' value='удалить отмеченные' />
</form>

<?php endif ?>