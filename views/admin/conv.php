<?php if (Admin_Model::is_admin_auth()):?>

<?php echo View::factory('admin/topmenu') ?>

<br>
<?php if ($ID):?>
<div class='adm_form'>
<span>Состав созыва:</span>
<form class='adm_form' action='/admin/save/dep_del' method='post'>
<table>
	<?php
		$res = Conv_Model::get_staff($ID);
		foreach ($res as $s): ?>
	<tr>
		<td><input type='checkbox' name='dep_del[]' value='<?=$s->dep_id?>' /></td>
		<td><a title='перейти к редактированию данных' href='/admin/deputy/<?=$s->dep_id?>'><?=$s->fio?></a></td>
	</tr>
	<?php
		endforeach;?>
</table>
<?php if (empty($res)):?>
<span><i>состав еще не определен</i></span>
<?php else: ?>
<input type='submit' title='исключить из состава созыва' value='исключить отмеченных' />
<input type='hidden' name='id' value='<?=$ID?>' />
<?php endif ?>
</form>
<hr>

<?php //echo '<p>todo форма, вывод списка депутатов(которых нет в созыве), кнопка добавить</p>' ?>
<form class='adm_form' action='/admin/save/dep_add/<?=$ID?>' method='post'>
	<?php
	$res = Conv_Model::deputies_not_in_conv($ID);
	if ($res): ?>
	<span>Добавить отмеченных депутатов в созыв:</span>
	<?php endif ?>
	
	<table>
	<?php
	foreach ($res as $d):?>
	<tr>
		<td><input type='checkbox' name='dep_add[]' value='<?=$d->dep_id?>' /></td>
		<td><?=$d->fio?></td>
	</tr>
	<?php endforeach ?>
	</table>
	<?php if ($res): ?>
	<input type='submit' title='добавить в состав созыва' value='добавить в состав' />
	<?php else: ?>
	<span><i>Все депутаты добавлены в созыв</i></span>
	<?php endif ?>
</form>

</div>
<?php else:?>
<form class='adm_form' action='/admin/save/conv_add' method='post'>
	<span>новый созыв:</span> <input type='text' name='conv_add' /> <input type='submit' value='добавить' />
</form>

<form class='adm_form' action='/admin/save/conv_del' method='post'>
<span>Список созывов:</span>
<table class='adm_form'>
<?php foreach (Conv_Model::get_convening() as $c):?>
	<tr>
		<td><input type='checkbox' name='conv_del[]' value='<?=$c->conv_id?>' /></td>
		<td><a title='редактировать состав' href='/admin/convocation/<?=$c->conv_id?>'><?=$c->conv_text?></td>
	</tr>
<?php endforeach ?>
</table>
<input type='submit' value='удалить отмеченные' />
</form>
<?php endif ?>

<?php else: ?>
	<b>Не авторизован</b> <br>
	<a href='/admin/login'>Войти</a>
<?php endif ?>