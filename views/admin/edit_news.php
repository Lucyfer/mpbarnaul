<?php
	$n = News_Model::get_news_data($ID);
	if ($n): ?>
<form class='adm_form' action='/admin/save/upd_news' method='post'>
	<input type='hidden' name='news[id]' value='<?=$ID?>' />

	<table>
		<tr><td>Заголовок:</td>			<td><input title='заголовок (теги не разрешены)' type='text' name='news[title]' value='<?=$n->news_title?>' size='100' /></td></tr>
		<tr><td>Краткое содержание:</td><td><input title='содержание (теги не разрешены)' type='text' name='news[desc]'  value='<?=$n->news_desc?>' size='100' /></td></tr>
	</table>

	<p>
		<span>Текст новости:</span>
		<br>
		<textarea id='area1' name='news[text]' cols='60' rows='20'><?=$n->news_text?></textarea>
	</p>
	<input type='submit' value='Сохранить изменения' />
</form>

<script type='text/javascript' src='/js/nicEdit.js'></script>
<script type="text/javascript"><!--
	bkLib.onDomLoaded(function() { new nicEditor({fullPanel : true}).panelInstance('area1'); });
	//-->
</script>

<?php else: ?>
<span>Новость не найдена</span>
<?php endif ?>