<?php echo View::factory('admin/topmenu') ?>

<?php
if ($ID): 
	$d = Conv_Model::get_deputy_info($ID);
	if ($d): ?>
<form class='adm_form' action='/admin/save/save_dep' method='post'>
	<span>Редактировать информацию:</span>
	<input type='hidden' name='dep[id]' value='<?=$d->dep_id?>' />
	<table class='adm_form'>
		<tr><td>ФИО:</td>		<td><input type='text' name='dep[fio]'   value='<?=$d->fio?>'   size='50' /></td></tr>
		<tr><td>Телефон:</td>	<td><input type='text' name='dep[phone]' value='<?=$d->phone?>' size='50' /></td></tr>
		<tr><td>E-mail:</td>	<td><input type='text' name='dep[mail]'  value='<?=$d->mail?>'  size='50' /></td></tr>
	</table>
	<input title='обновить информацию о депутате' type='submit' value='Сохранить' />
</form>
<?php else: ?>
<p>Ничего не найдено</p>
<?php endif ?>

<?php else: ?>
<form class='adm_form' action='/admin/save/new_dep' method='post'>
	<span>Новый депутат:</span>
	<table class='adm_form'>
		<tr><td>ФИО:</td>		<td><input type='text' name='dep[]' size='50' /></td></tr>
		<tr><td>Телефон:</td>	<td><input type='text' name='dep[]' size='50' /></td></tr>
		<tr><td>E-mail:</td>	<td><input type='text' name='dep[]' size='50' /></td></tr>
	</table>
	<input type='submit' value='Добавить' />
</form>

<hr>

<table class='adm_form'>
 <form action='/admin/save/drop_dep' method='post'>
<?php  foreach (Conv_Model::get_all_deputies() as $d): ?>
	<tr>
		<td>
		<input type='checkbox' name='dep[]' value='<?=$d->dep_id?>'><a title='перейти к редактированию' href='/admin/deputy/<?=$d->dep_id?>'><?=$d->fio?></a><br><br>
		</td>
	</tr>
<?php endforeach ?>
 <tr>
   <td>
<input type='submit' value='удалить отмеченных'>
   </td>
</tr>
</form>
</table>
<?php endif ?>