<?php if (Admin_Model::is_admin_auth()):?>

<?php echo View::factory('admin/topmenu') ?>
	
<form action='/admin' method='post'>
</form>

<?php else: ?>
	<b>Не авторизован</b> <br>
	<a href='/admin/login'>Войти</a>
<?php endif ?>