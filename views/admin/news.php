<?php echo View::factory('admin/topmenu') ?>

<?php
if ($ID):
	echo View::factory('admin/edit_news')->set('ID',$ID)->render();
else:?>

<form class='adm_form' action='/admin/save/del_news' method='post'>
	<p>
		<a href='/admin/add_news'>Создать новость</a>
	</p>
	<table>
	<?php foreach (News_Model::get_all() as $n): ?>
		<tr>
			<td><input type='checkbox' name='del[]' value='<?=$n->news_id?>' /></td>
			<td><span><?=News_Model::format_date($n->news_date)?></span></td>
			<td><a title='редактировать новость' href='/admin/news/<?=$n->news_id?>'><?=$n->news_title?></a></td>
		</tr>
	<?php endforeach ?>
	</table>
	<input type='submit' value='Удалить отмеченные' />
</form>

<?php endif ?>