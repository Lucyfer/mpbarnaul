<html>
<style>
P>span {
	font-size: 0.7em;
}
</style>
<body>
	<h3>Результаты поиска</h3>
	
<?php
	if ($news == null): ?>
	<p>Ничего не найдено</p>
<?php 
	else: 
	foreach ($news as $n): ?>
	<p>
		<b><?=$n->news_title?></b>
		<br />
		<span><?=$n->news_desc?></span>
	</p>
<?php
	endforeach;
	endif; ?>

</body>
</html>