<?='<?xml version="1.0" encoding="UTF-8"?>'?>

<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
    <channel>
        <title>RSS канал <?=$url?></title>
        <link>http://<?=$url?>/news/rss</link>
        <description><![CDATA[Новости проекта]]></description>
        <language>ru-ru</language>
        <managingEditor>X-Union Developers</managingEditor>
        <generator>rss engine &amp; Kohana</generator>
        <pubDate><?=date('r')?></pubDate>
        <lastBuildDate><?=date('r', $last_date ? strtotime($last_date) : time())?></lastBuildDate>
        <image>
            <link>http://<?=$url?>/news/rss</link>
            <url>http://<?=$url?>/img/logo.png</url>
            <title>RSS канал <?=$url?></title>
        </image>
        <atom:link href="http://<?=$url?>/news/rss" rel="self" type="application/rss+xml" />
<?php foreach($news as $n): ?>

        <item>
            <title><?= strip_tags($n->news_title) ?></title>
            <guid isPermaLink="true">http://<?=$url?>/news/content/<?=$n->news_id?></guid>
            <link>http://<?=$url?>/news/content/<?=$n->news_id?></link>
            <description>
                <![CDATA[ <?=$n->news_desc?> ]]>
            </description>
            <pubDate><?=date('r', strtotime($n->news_date))?></pubDate>
            <author>admin@<?=$url?> (admin)</author>
        </item>
<?php endforeach ?>
<?php if (!$news): ?>

        <item>
            <title>Новостей еще нет</title>
            <guid isPermaLink="true">http://<?=$url?>/</guid>
            <link>http://<?=$url?>/news/</link>
            <description>
                <![CDATA[ Новостей еще нет ]]>
            </description>
            <pubDate><?= date('r') ?></pubDate>
            <author>admin@<?=$url?> (admin)</author>
        </item>
<?php endif ?>
    </channel>
</rss>