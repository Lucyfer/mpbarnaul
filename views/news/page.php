<?php
/*
<html>
<body>
<h3>Список новостей</h3>
*/
?>

<?php
if ($news): ?>

<?=$this->pagination->render()?>

<?php
/*
<table>
<? foreach($news as $n): ?>
	<tr>
		<td><?= //date('Y-m-d H:i', $n->news_date) 
			date('[d.m.Y]', strtotime($n->news_date)) ?></td>
		<td><a href='/news/content/<?=$n->news_id?>'><?=$n->news_title?></a></td>
	</tr>
<? endforeach ?>
</table>
*/
?>

<? foreach($news as $n): ?>
<p>
	<span class='date'><?=date('[d.m.Y]', strtotime($n->news_date))?></span><br/>
	<span class='header'><?=$n->news_title?></span>
	<div class='content'>
		<?=$n->news_desc?>		
		<div class='next'><a href='/news/content/<?=$n->news_id?>'>далее &rarr;</a></div>
	</div>
</p>

<?php
  endforeach;
else:?>
	<p>ничего не найдено</p>
<?php
endif; ?>

<?php
/*
</body>
</html>
*/
?>