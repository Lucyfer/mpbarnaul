<?php die('obsolete'); ?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>' ?>

<rss version="2.0">
<channel>
<title>RSS канал <?=$url?></title>
<link>http://<?=$url?></link>
<description>Новости проекта</description>
<language>ru-ru</language>
<? /*
<image>
<url>http://<?=$url?>/icons/logo.png</url>
<title>favicon</title>
<link>http://<?=$url?></link>
</image>
*/ ?>

<lastBuildDate><?=date('r', $last_date)?></lastBuildDate>

<?php foreach($news as $n): ?>
<item>
	<title><?=$n->news_title?></title>
	<link>http://<?=$url?>/content/news/index.php?nid=$row[0]</link>
	<description><?=$n->news_desc?></description>
	<pubDate><?=date('r', $n->news_date)?></pubDate>
	<guid isPermaLink='true'>http://<?=$url?>/content/news/index.php?nid=$row[0]</guid>
</item>
<?php endforeach ?>

</channel>
</rss>
