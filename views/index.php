<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="keywords" content="молодежный парламент , политика , Барнаул , новости парламента , молодежная политика">
    <title>&bull; Молодежный парламент &bull;</title>
	<link title="rss канал <?=$_SERVER['SERVER_NAME']?>" type="application/rss+xml" rel="alternate" href="http://<?=$_SERVER['SERVER_NAME']?>/news/rss">
    <link type="text/css" rel="stylesheet" href="/style/main.css">
	<!--[if IE]>
		<link type="text/css" rel="stylesheet" href="/style/ie_hack.css">
	<![endif]-->
  </head>
  <body>
  <table cellpadding="0" cellspacing="0" class='top'>
   <tr>
    <td width='100%'>

	  <!-- шапка   -->
	  <table cellpadding="0" cellspacing="0" class='top'>
       <tr>
	     <td>

		    <!--  строка навигации   -->
		     <div class="top_left">
			    <div class="top_left_bot">
					<a href='/' class='nav'>Главная</a><?php
						//$nav = array('/news'=>'Новости', '/news/rss'=>'rss канал');
						if ($nav)
							foreach ($nav as $k=>$v):?><font class="nav"> &rarr; </font><a href='<?=$k?>' class='nav'><?=$v?></a><?php endforeach?>
				</div>
			 </div>
			<!-- -->

		 </td>
		 <td class='top'></td>
		 <td>
		     <div class="top_right"></div>
		 </td>
	   </tr>
     </table>
	 <!--     -->

	</td>
   </tr>

	 <tr>
	   <td width="100%">

	 <!-- середина с контентом -->
      <table cellpadding="0" cellspacing="0" class='top'>
       <tr>
	      <td class='lft' valign='top'>

			  <div class='menu'>

			  <!-- контейнер c меню   -->
<table border=0 cellspacing=0 cellpadding=0>

<!--        1 строка  -->
<tr>
<td height=15><img src="/img/left_top/top_01.png" height=15 alt='top'></td>       <!-- 1 ячейка    -->
<td style="background:url(/img/left_top/top_02.png)" ></td>                 <!-- 2 ячейка    -->
<td height=15><img src="/img/left_top/top_03.png" height=15 alt='top'></td>           <!-- 3 ячейка    -->
</tr>
<!--                  -->

<!--        2 строка  -->
<tr>
<td style="background-image:url(/img/left_top/centr_04.png)" width=15>&nbsp;</td>             <!-- 1 ячейка    -->
<td valign="top" style="background:url(/img/left_top/centr_05.png)">                 <!-- меню    -->
     <?php  $t = Kohana::config('config');?>
     <p class="but">
	    <a href='<?php echo 'http://'.$t['site_domain'].'/reception';?>' class='menu'>Виртуальная приемная</a>
	 </p>
	 <p class="but">
		<a href='<?php echo 'http://'.$t['site_domain'].'/info';?>' class='menu'>Созыв</a>
	 </p>
	 <p class="but">
		<a href='<?php echo 'http://'.$t['site_domain'].'/workgroup';?>' class='menu'>Рабочие группы</a>
	 </p>
	 <p class="but">
		<a href='<?php echo 'http://'.$t['site_domain'].'/archive';?>' class='menu'>Документы</a>
	 </p>
	 <p class="but">
		<a href='<?php echo 'http://'.$t['site_domain'].'/about';?>' class='menu'>О парламенте</a>
	 </p>
	 <p class="but">
		<a href='<?php echo 'http://'.$t['site_domain'].'/videoblog';?>' class='menu'>Видеоблог</a>
	 </p>
</td>
<td style="background-image:url(/img/left_top/centr_06.png)" width=15>&nbsp;</td>            <!-- 3 ячейка    -->
</tr>
<!--          -->

<!--        3 строка  -->
<tr>
<td height=15><img src="/img/left_top/bot_07.png" height=15 alt='bottom'></td>           <!-- 1 ячейка    -->
<td style="background-image:url(/img/left_top/bot_08.png)"></td>                                  <!-- 2 ячейка    -->
<td height=15><img src="/img/left_top/bot_09.png" height=15 alt='bottom'></td>           <!-- 3 ячейка    -->
</tr>
<!--          -->
</table>
<!-- Конец контейнера    -->

<!-- контейнер c новостями  -->
<table border=0 cellspacing=0 cellpadding=0>

<!--        1 строка  -->
<tr>
<td height=15><img src="/img/left_bot/top1.png" height=15 alt='top'></td>       <!-- 1 ячейка    -->
<td style="background:url(/img/left_bot/top2.png)" ></td>                 <!-- 2 ячейка    -->
<td height=15><img src="/img/left_bot/top3.png" height=15 alt='top'></td>           <!-- 3 ячейка    -->
</tr>
<!--                  -->

<!--        2 строка  -->
<tr>
<td style="background-image:url(/img/left_bot/cntr1.png)" width=15>&nbsp;</td>             <!-- 1 ячейка    -->
<td valign="top" style="background:url(/img/left_bot/cntr2.png); white-space:normal; " width="190">
<!-- меню    -->

    <table cellpadding="0" cellspacing="0" border="0" width="190" style="overflow:hidden; display:block;">
	 <tr>
	  <td style="table-layout:fixed;">
		<h3>Новости</h3> <!-- левая колонка -->

		<?php echo View::factory('last_news') ?>

		</td>
	  </tr>
	</table>

</td>
<td style="background-image:url(/img/left_bot/cntr3.png)" width=15>&nbsp;</td>            <!-- 3 ячейка    -->
</tr>
<!--          -->


<!--        3 строка  -->
<tr>
<td height=15><img src="/img/left_bot/bot1.png" height=15 alt='bottom'></td>           <!-- 1 ячейка    -->
<td style="background-image:url(/img/left_bot/bot2.png)"></td>                                  <!-- 2 ячейка    -->
<td height=15><img src="/img/left_bot/bot3.png" height=15 alt='bottom'></td>           <!-- 3 ячейка    -->
</tr>
<!--          -->
</table>
 <!-- Конец контейнера    -->

   </div>

          </td>

		  <td class="center" width="100%">

		   <br>
			 	 <!--  контейнер с основным содержимым   -->

			 <table border=0 cellspacing=0 cellpadding=0  width="100%">

<!--        1 строка  -->
<tr>
<td height=20><img src="/img/center/cntr1_top.png" height=20 alt='top'></td>       <!-- 1 ячейка    -->
<td style="background:url(/img/center/cntr2_top.png)" ></td>                 <!-- 2 ячейка    -->
<td height=20><img src="/img/center/cntr3_top.png" height=20 alt='top'></td>           <!-- 3 ячейка    -->
</tr>
<!--                  -->

<!--        2 строка  -->
<tr>
<td style="background-image:url(/img/center/cntr1_mid.png)" width=20>&nbsp;</td>             <!-- 1 ячейка    -->
<td style="background:url(/img/center/cntr2_mid.png);">                 <!-- основное содержимое   -->
	<h2><?=$section?></h2>

	<?php echo $child_view ?>

</td>
<td style="background-image:url(/img/center/cntr3_mid.png)" width=20>&nbsp;</td>            <!-- 3 ячейка    -->
</tr>
<!--          -->

<!--        3 строка  -->
<tr>
<td height=20><img src="/img/center/cntr1_bot.png" height=20 alt='center'></td>           <!-- 1 ячейка    -->
<td style="background-image:url(/img/center/cntr2_bot.png)"></td>                                  <!-- 2 ячейка    -->
<td height=20><img src="/img/center/cntr3_bot.png" height=20 alt='center'></td>           <!-- 3 ячейка    -->
</tr>
<!--          -->
</table>

			 <!--  -->

          </td>

          <td class='rht' valign='top'>
		      <div class="rgh">

				<!-- контейнер c поиском  -->
<table border=0 cellspacing=0 cellpadding=0>

<!--        1 строка  -->
<tr>
<td height=15><img src="/img/left_top/top_01.png" height=15 alt='top'></td>       <!-- 1 ячейка    -->
<td style="background:url(/img/left_top/top_02.png)" ></td>                 <!-- 2 ячейка    -->
<td height=15><img src="/img/left_top/top_03.png" height=15 alt='top'></td>           <!-- 3 ячейка    -->
</tr>
<!--                  -->

<!--        2 строка  -->
<tr>
<td style="background-image:url(/img/left_top/centr_04.png)" width="15">&nbsp;</td>             <!-- 1 ячейка    -->
<td valign="top" style="background:url(/img/left_top/centr_05.png)" width="100" align="right">                 <!-- поиск   -->
<?php
/*
    <form action="#" method="post" class='search'>
	    <input type="text" class='search'>
		<input type="submit" value="поиск" class='search_but'>
	 </form>
*/
	 echo View::factory('search/nigma') ?>
</td>
<td style="background-image:url(/img/left_top/centr_06.png)" width=15>&nbsp;</td>            <!-- 3 ячейка    -->
</tr>
<!--          -->

<!--        3 строка  -->
<tr>
<td height=15><img src="/img/left_top/bot_07.png" height=15 alt='bottom'></td>           <!-- 1 ячейка    -->
<td style="background-image:url(/img/left_top/bot_08.png)"></td>                                  <!-- 2 ячейка    -->
<td height=15><img src="/img/left_top/bot_09.png" height=15 alt='bottom'></td>           <!-- 3 ячейка    -->
</tr>
<!--          -->
</table>
 <!-- Конец контейнера    -->


			  </div>
          </td>
	   </tr>
     </table>
	 <!--   -->

    </td>
  </tr>

  <tr>
	<td colspan='3' width='100%'>
		<table  width='100%'>
			<tr align='center'><td class='bottom_l1'>::&nbsp;<a href='/map' class='bottom'>карта сайта</a>&nbsp;::&nbsp;<a href='/search' class='bottom'>поиск</a>&nbsp;::</td></tr>
			<tr align='right'>
				<td class='bottom_l2' height="58" style="">
				<font class="author">создание сайта &copy;&nbsp</font><a href='http://staremax.ru' class='author'>StareMax</a>&nbsp;
<a href="http://validator.w3.org/check?uri=http://mpbarnaul.ru"><img src="http://www.w3.org/Icons/valid-html40-blue" alt="Valid HTML 4.0 Transitional" height="31" width="88" border="0" align='middle'></a>&nbsp;
<a href="http://jigsaw.w3.org/css-validator/validator?uri=http://mpbarnaul.ru"><img style="border:0;width:88px;height:31px" src="http://jigsaw.w3.org/css-validator/images/vcss-blue" alt="CSS Is Check!" border="0" align='middle'></a>&nbsp;
<a href="http://validator.w3.org/feed/check.cgi?url=http://mpbarnaul.ru/news/rss"><img style='border:0' src="/img/valid-rss.png" alt="[Valid RSS]" title="Validate my RSS feed" align='middle'></a>
				</td>
			</tr>
		</table>
	</td>
  </tr>
</table>

</body>
</html>

