<?php /* последние 2 новости / для левой колонки */
	$m = new News_Model;
	$news = $m->get_news(2);
	if ($news):
	foreach ($news as $n):?>

		<p>
			<span class='date'><?=date('[d.m.Y]', strtotime($n->news_date))?></span><br>
			<span class='header'><?=(mb_strlen($n->news_title)>60) ? mb_substr($n->news_title,0,60).'...' : $n->news_title?></span>
			<div>
				<a class='news' href='/news/content/<?=$n->news_id?>'><?=$n->news_desc?></a>
			</div>
		</p>
<?php endforeach;
	else:?>
	<p>
	пока нет новостей
	</p>
<?php endif ?>