<?php
if ($docs):
	foreach ($docs as $d): ?>
<p>
	<span class='date'><?=date('[d.m.Y]', strtotime($d->doc_date))?></span>
	<br/>
	<b><a href='/archive/doc/<?=$d->doc_id?>'><?=$d->doc_title?></a></b>
</p>
<?php
	endforeach;
	
	echo $this->pagination;
else: ?>
<p>Пока нет документов</p>
<?php 
endif; ?>