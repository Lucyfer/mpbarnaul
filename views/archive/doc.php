<?php if ($doc): ?>
<p>
	<span class='date'><?=date('[d.m.Y]', strtotime($doc->doc_date))?></span><br/>
	<span class='header'><?=$doc->doc_title?> </span>

	<p>
	<?=$doc->doc_text?>
	</p>
</p>
<?php else: ?>
<p>Не найден</p>
<?php endif; ?>