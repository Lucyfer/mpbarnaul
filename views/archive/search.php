<html>
<style>
P>span {
	font-size: 0.7em;
}
</style>
<body>
	<h3>Результаты поиска</h3>
	
<?php
	if ($docs == null): ?>
	<p>Ничего не найдено</p>
<?php 
	else: 
	foreach ($docs as $n): ?>
	<p>
		<b><?=$n->doc_title?></b>
		<br />
		<span><?=$n->doc_text?></span>
	</p>
<?php
	endforeach;
	endif; ?>

</body>
</html>