<?php defined('SYSPATH') OR die('No direct access allowed.');

define('MAX_NEWS_ON_PAGE', 35);

class News_Controller extends Template_Controller {
	public $template = 'index';

	public function index()
	{
		url::redirect('/news/page');
	}

	// список новостей
	public function page($page = 1)
	{
		$news = new News_Model;
		$page = intval($page) - 1;
		if ($page < 0) $page = 0;

		$this->template->nav = array('/news' => 'Новости', '/news/page/'.($page+1) => 'Страница #'.($page+1));
		$this->template->section = 'Новостная лента';
		$this->template->child_view = new View('news/page');

		$count = $news->get_news_count();

		$this->pagination = new Pagination(array(
			'uri_segment'    => 'page',
			'total_items'    => $count,
			'items_per_page' => MAX_NEWS_ON_PAGE,
			'style'          => 'punbb'
		));

		$this->template->child_view->news =
			$news->get_news(MAX_NEWS_ON_PAGE, MAX_NEWS_ON_PAGE * $page);
	}

	// rss канал
	public function rss()
	{
		$news = new News_Model;
		header('Content-Type: application/rss+xml; charset=UTF-8', true);
		$this->template = new View('news/rss2');
		$this->template->news = $news->get_news();
		$this->template->last_date = $news->get_last_date();
		$this->template->url = $_SERVER['SERVER_NAME'];
	}

	// содержимое новости
	public function content($id = null)
	{
		$news = new News_Model;

		$this->template->nav = array('/news' => 'Новости', '/news/content/'.$id => 'Новость #'.$id);
		$this->template->section = 'Новостная лента';
		$this->template->child_view = new View('news/content');
		$this->template->child_view->data = $id ? $news->get_news_data($id) : null;
	}

	// поиск по название или дате
	public function search($where=null)
	{
		if (isset($_POST['date']) || isset($_POST['title']))
		{
			$this->template = new View('news/search');
			$news = new News_Model;
			switch ($where)
			{
				case 'date':
					$this->template->news = $news->search($_POST['date'], News_Model::DATE);
					break;

				case 'title':
					$this->template->news = $news->search($_POST['title'], News_Model::TITLE);
					break;

				default: url::redirect('/news');
			}
		}
		else // редиректим на список новостей
			url::redirect('/news');
	}
}
?>