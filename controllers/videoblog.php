<?php defined('SYSPATH') OR die('No direct access allowed.');

define('MAX_DOCS_ON_PAGE', 35);

class Videoblog_Controller extends Template_Controller {

	public $template = 'index';

	public function index()
	{
		$this->template->nav = array('/videoblog' => 'Видеоблог');
		$this->template->section = 'Видеоблог';
		$this->template->child_view = new View('videoblog/index');
	}
}