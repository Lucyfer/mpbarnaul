<?php defined('SYSPATH') OR die('No direct access allowed.');

class Info_Controller extends Template_Controller {

	public $template = 'index';

	// список созывов
	public function index()
	{
		$c = new Conv_Model();
		$this->template->nav = array('/info' => 'Созывы');
		$this->template->section = 'Информация о созывах';
		$this->template->child_view = new View('info/index');
		$this->template->child_view->conv = $c->get_convening();
	}

	// Информация о созыве (информация о депутатах нынешнего созыва , их контакты)
	public function convocation($num = NULL)
	{
		$c = new Conv_Model();
		$this->template->nav = array('/info' => 'Созывы', '/info/convocation/'.$num => 'Информация об этом созыве');
		$this->template->section = 'Информация о созывах';
		$this->template->child_view = new View('info/conv');
		$this->template->child_view->staff = $c->get_staff($num);
	}

	// информация о депутате
	public function deputy($id = NULL)
	{
		$c = new Conv_Model();
		$this->template->nav = array('/info' => 'Созывы', '/info/deputy/'.$id => 'Информация о депутате');
		$this->template->section = 'Информация о депутате';
		$this->template->child_view = new View('info/deputy');
		$this->template->child_view->dep = $c->get_deputy_info($id);
	}
}
?>