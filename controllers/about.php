<?php defined('SYSPATH') OR die('No direct access allowed.');

class About_Controller extends Template_Controller {

	public $template = 'index';

	public function index()
	{
		$this->template->nav = array('/about' => 'О парламенте');
		$this->template->section = 'Информация о парламенте';
		$this->template->child_view = new View('about');
	}
}