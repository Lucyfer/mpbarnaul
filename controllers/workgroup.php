<?php defined('SYSPATH') OR die('No direct access allowed.');

class Workgroup_Controller extends Template_Controller {

	public $template = 'index';

	public function index()
	{
		$w = new Workgroup_Model;
		$this->template->nav = array('/workgroup' => 'Рабочие группы');
		$this->template->section = 'Рабочие группы';
		$this->template->child_view = new View('workgroup/index');
		$this->template->child_view->work = $w->get_groups();
	}

	public function group($ID = NULL)
	{
		$w = new Workgroup_Model;
		$this->template->nav = array('/workgroup' => 'Рабочие группы', '/workgroup/group/'.$ID => 'Состав');
		$this->template->section = 'Состав рабочей группы';
		$this->template->child_view = new View('workgroup/group');
		$this->template->child_view->staff = $w->get_members($ID);
	}
}