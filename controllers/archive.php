<?php defined('SYSPATH') OR die('No direct access allowed.');

define('MAX_DOCS_ON_PAGE', 35);

class Archive_Controller extends Template_Controller {

	public $template = 'index';

	// список документов
	public function index()
	{
		url::redirect('/archive/page');
	}

	public function page($page = 1)
	{
		$d = new Arch_Model;
		$page = intval($page) - 1;
		if ($page < 0) $page = 0;

		$count = $d->get_docs_count();

		$this->pagination = new Pagination(array(
			'uri_segment'    => 'page',
			'total_items'    => $count,
			'items_per_page' => MAX_DOCS_ON_PAGE,
			'style'          => 'punbb'
		));

		$this->template->nav = array('/archive' => 'Архив', '/archive/page/'.($page+1) => 'Страница #'.($page+1));
		$this->template->section = 'Архив документов';
		$this->template->child_view = new View('archive/index');
		$this->template->child_view->docs = $d->get_docs(MAX_DOCS_ON_PAGE, MAX_DOCS_ON_PAGE*$page);
	}

	// информация о документе
	public function doc($id = null)
	{
		if (intval($id) <= 0) url::redirect('/archive');

		$d = new Arch_Model;
		$this->template->nav = array('/archive' => 'Архив', '/archive/doc/'.$id => 'Документ #'.$id);
		$this->template->section = 'Архив документов';
		$this->template->child_view = new View('archive/doc');
		$this->template->child_view->doc = $d->get_doc_info($id);
	}

	// поиск по заголовку, телу, дате документа
	public function search($where=null)
	{
		if (isset($_POST['date']) || isset($_POST['title']))
		{
			$this->template = new View('archive/search');
			$a = new Arch_Model();

			switch ($where)
			{
				case 'date':
					$this->template->docs = $a->search_docs($_POST['date'], Arch_Model::DATE);
					break;

				case 'title':
					$this->template->docs = $a->search_docs($_POST['title'], Arch_Model::TITLE);
					break;
/* пока отключим
				case 'body':
					$this->template->docs = $a->search_docs($_POST['body'], Arch_Model::BODY);
					break;
*/
				default: url::redirect('/archive');
			}
		}
		else url::redirect('/archive');
	}
}