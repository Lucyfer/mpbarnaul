<?php defined('SYSPATH') OR die('No direct access allowed.');

class Search_Controller extends Template_Controller {
	public $template = 'index';

	public function index()
	{
		$this->template->nav = array('/search' => 'Поиск по сайту');
		$this->template->section = 'Поиск по сайту';
		$this->template->child_view = new View('search/index');
	}
}
