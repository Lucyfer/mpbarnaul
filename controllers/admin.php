<?php defined('SYSPATH') OR die('No direct access allowed.');

class Admin_Controller extends Template_Controller {

	public $template = 'index';

	public function index()
	{
		$this->template->nav = array('/admin' => 'Админка');
		$this->template->section = 'Админка';
		$this->template->child_view = new View('admin/index');
	}
	
	public function logout()
	{
		Admin_Model::logout();
		url::redirect('/admin/login');
	}
	
	public function login()
	{
		$this->template->nav = (array('/admin' => 'Админка', '/admin/login' => 'Вход'));
		$this->template->section = 'Админка/Вход';
		$this->template->child_view = new View('admin/login');
		$this->template->child_view->retry = false;

		if (isset($_POST['login'], $_POST['passw']))
		{
			$a = Admin_Model::check_admin_auth($_POST['login'], $_POST['passw']);
			if ($a)
			{
				Admin_Model::set_admin_auth($a->admin_id);
				url::redirect('/admin');
			}
			else
				$this->template->child_view->retry = true;
		}
	}

	/**
	 * админка созыв
	 *
	 */
	public function convocation($ID = NULL)
	{
		$this->template->nav = array('/admin' => 'Админка', '/admin/convocation' => 'Созывы');
		$this->template->section = 'Админка';
		$this->template->child_view = new View('admin/conv');
		$this->template->child_view->ID = intval($ID);
	}

	public function group($ID = NULL)
	{
		$this->template->nav = array('/admin' => 'Админка', '/admin/group' => 'Группы');
		$this->template->section = 'Админка';
		if (Admin_Model::is_admin_auth())
		{
			$this->template->child_view = new View('admin/group');
			$this->template->child_view->ID = intval($ID);
		}
		else
			$this->template->child_view = new View('admin/noauth');
	}

	public function deputy($ID = NULL)
	{
		$this->template->nav = array('/admin' => 'Админка', '/admin/deputy' => 'Депутаты');
		$this->template->section = 'Админка';
		if (Admin_Model::is_admin_auth())
		{
			$this->template->child_view = new View('admin/deputy');
			$this->template->child_view->ID = intval($ID);
		}
		else
			$this->template->child_view = new View('admin/noauth');
	}

	public function docs($ID = NULL)
	{
		if (!Admin_Model::is_admin_auth())
			url::redirect('/admin/login');
		$this->template->nav = array('/admin' => 'Админка', '/admin/docs' => 'Документы');
		$this->template->section = 'Админка';
		if (Admin_Model::is_admin_auth())
		{
			$this->template->child_view = new View('admin/docs');
			$this->template->child_view->ID = intval($ID);
		}
		else
			$this->template->child_view = new View('admin/noauth');
	}

	public function new_doc()
	{
		$this->template->nav = array('/admin' => 'Админка', '/admin/new_doc' => 'Новый документ');
		$this->template->section = 'Админка';
		if (Admin_Model::is_admin_auth())
			$this->template->child_view = new View('admin/new_doc');
		else
			$this->template->child_view = new View('admin/noauth');
	}

	public function news($ID = NULL)
	{
		$this->template->nav = array('/admin' => 'Админка', '/admin/news' => 'Новости');
		$this->template->section = 'Админка';
		if (Admin_Model::is_admin_auth())
		{
			$this->template->child_view = new View('admin/news');
			$this->template->child_view->ID = $ID;
		}
		else
			$this->template->child_view = new View('admin/noauth');
	}

	public function add_news()
	{
		$this->template->nav = array('/admin' => 'Админка', '/admin/add_news' => 'Создать новость');
		$this->template->section = 'Админка';
		if (Admin_Model::is_admin_auth())
			$this->template->child_view = new View('admin/add_news');
		else
			$this->template->child_view = new View('admin/noauth');
	}
	public function about()
	{
	    $this->template->nav = array('/admin' => 'Админка', '/admin/about' => 'Изменение информации о парламенте');
		$this->template->section = 'Админка';
		if (Admin_Model::is_admin_auth())
			$this->template->child_view = new View('admin/about');
		else
			$this->template->child_view = new View('admin/noauth');
	
	}

	public function save($action, $data=null)
	{
	   $r = Admin_Model::get_admin_login(Admin_Model::is_admin_auth());
		switch ($action)
		{
		   // создать новый созыв
			case 'upd_info':
				if (isset($_POST['txt']))
				   {
				     if (Admin_Model::check_write_info($r->admin_login))
					   Database::instance()->query("update about set text='".$_POST['txt']."' where id=1 ");
					  else die('Error : Вы не имеете прав на изменение информации');
					}
				url::redirect('/admin');
				break;
		
			// создать новый созыв
			case 'conv_add':
				if (isset($_POST['conv_add']))
				   {
				     if (Admin_Model::check_write_convocations($r->admin_login))
					  Conv_Model::add_conv($_POST['conv_add']);
					  else die('Error : Вы не имеете прав на добавление созыва');
					}
				url::redirect('/admin/convocation');
				break;

			// удаление созыва
			case 'conv_del':
				if (isset($_POST['conv_del']))
				  {
				     if (Admin_Model::check_write_convocations($r->admin_login))
					        Conv_Model::del_conv($_POST['conv_del']);
					 else die('Error : Вы не имеет прав на удаление созыва'); 		
				  }
				url::redirect('/admin/convocation');
				break;

			// создание группы
			case 'group_add':
				if (isset($_POST['group_add']))
				  {
				    if (Admin_Model::check_write_workgroup($r->admin_login))
					    Workgroup_Model::add_group($_POST['group_add']);
					 else die('Error : Вы не имеете прав на добавление рабочей группы');	
				   }
				url::redirect('/admin/group');
				break;

			// удаление группы
			case 'group_del':
				if (isset($_POST['group_del']))
				   {
				     if (Admin_Model::check_write_workgroup($r->admin_login))
					  Workgroup_Model::del_group($_POST['group_del']);
					  else die('Error : Вы не имеете прав на удаление рабочей группы');
					}
				url::redirect('/admin/group');
				break;

			// удаление депутата из созыва
			case 'dep_del':
				$r = '/admin/convocation/';
				  {
				   if (Admin_Model::check_write_convocations($r->admin_login)){
				if (isset($_POST['dep_del'],$_POST['id']))
					Conv_Model::del_from_conv($_POST['id'], $_POST['dep_del']);
				if (isset($_POST['id']))
					$r .= intval($_POST['id']);
					 }
					 else die('Error : Вы н еимеете прав на изменение созыва');
					}
				url::redirect($r);
				break;

			// удаление депутата из группы
			case 'del_from_gr':
				$r = '/admin/group/';
				$data = intval($data);
				if ($data > 0)
				{
				   if (Admin_Model::check_write_workgroup($r->admin_login)){
					if (isset($_POST['dep_del']))
						Conv_Model::del_from_group($data, $_POST['dep_del']);
					$r .= intval($data);
					}
					else die('Error : Вы не имеете прав на изменение рабочей группы');
				}
				url::redirect($r);
				break;

			// добавление депутата в созыв
			case 'dep_add':
				if (isset($_POST['dep_add']) && $data)
				   {
				     if (Admin_Model::check_write_convocations($r->admin_login)){
					Conv_Model::add_deput_to_conv($data, $_POST['dep_add']);
					  }
					  else die('Error : Вы не имеете прав на изменение созыва');
					}
				url::redirect('/admin/convocation/'.intval($data));
				break;

			// добавить депутата в групy
			case 'add_to_gr':
				if (isset($_POST['dep_add']) && $data)
				{
				   if (Admin_Model::check_write_workgroup($r->admin_login)){
					Conv_Model::add_deput_to_group($data, $_POST['dep_add']);
					}
					else die('Error : Вы н еимеет прав на изменение рабочей группы');
				}
				url::redirect('/admin/group/'.intval($data));
				break;

			// создать нового депутата
			case 'new_dep':
				if (isset($_POST['dep']))
				  {
				    if (Admin_Model::check_edit_deput($r->admin_login))
					 {
					if (!Conv_Model::add_new_deputy($_POST['dep']))
						die('Error: Заполните все поля');
						}
					  else die('Error : Вы не имеет прав на добавление депутата');	
				  }
				url::redirect('/admin/deputy');
				break;

			// обновить инфо о депутате
			case 'save_dep':
				if (isset($_POST['dep']))
				  {
				    if (Admin_Model::check_edit_deput($r->admin_login))
					  {
					if (!Conv_Model::update_dep($_POST['dep']))
						die('Error: Заполните все поля');
						}
						else die('Error : Вы не имеете прав на изменение информации о депутате');
				  }
				url::redirect('/admin/deputy');
				break;
				
			// удалить депутата из базы
			case 'drop_dep':
				if (isset($_POST['dep']))
				  {
				    if (Admin_Model::check_edit_deput($r->admin_login))
					     Conv_Model::drop_dep($_POST['dep']);
						else die('Error : Вы не имеете прав на удаление депутата');
				  }
				  url::redirect('/admin/deputy');
				break;	

			// обновить документ
			case 'doc_upd':
				if (isset($_POST['doc']))
				 {
				   if (Admin_Model::check_write_archiv($r->admin_login))
				    {
					if (!Arch_Model::update_doc($data,$_POST['doc']))
						die('Error: Заполните все поля');
						}
						else die('Error : Вы не имеете прав на обновление документа');
				  }		
				url::redirect('/admin/docs');
				break;

			// удалить документы
			case 'del_doc':
				if (isset($_POST['doc']))
				  {
				    if (Admin_Model::check_write_archiv($r->admin_login))
					Arch_Model::del_doc($_POST['doc']);
					 else die('Error : Вы не имеете прав на удаление документа');
					}
				url::redirect('/admin/docs');
				break;

			// создать новый документ
			case 'new_doc':
				if (isset($_POST['doc']))
				   {
				     if (Admin_Model::check_write_archiv($r->admin_login))
					  {
					if (!Arch_Model::add_doc($_POST['doc']))
						die('Error: Заполните все поля');
						}
						else die('Error : Вы не имеете прав на добавление документа');
					}
				url::redirect('/admin/docs');
				break;

			// обновить новость
			case 'upd_news':
				if (isset($_POST['news']))
				 {
				  if(Admin_Model::check_write_news($r->admin_login))
				    {
					  if (!News_Model::update_news($_POST['news']))
						die('Error: Заполните все поля');
					}
				  else 	die('Error : Вы не имеет прав на изменение новости');	
				 }		
				url::redirect('/admin/news');
				break;

			// удаление новостей
			case 'del_news':
				if (isset($_POST['del']))
				  {
				     if(Admin_Model::check_write_news($r->admin_login))
				    {
					if(!News_Model::delete_news($_POST['del']))
					  die('Error : Не удалось удалить новость');
					  }
					  else die ('Error : Вы не имеет прав на удаление новости');
				  }	  
				url::redirect('/admin/news');
				break;

			// создать новость
			case 'add_news':
				if (isset($_POST['news']))
				 {
				  if (Admin_Model::check_write_news($r->admin_login))
				    {
					  if (!News_Model::add_news($_POST['news']))
						die('Error: Заполните все поля');
					}
					else die('Error : Вы не имеет прав на добавление новости');	
				 }	
				url::redirect('/admin/news');
				break;
		}
		die();
	}
}