<?php defined('SYSPATH') OR die('No direct access allowed.');

class Reception_Controller extends Template_Controller {
	public $template = 'index';

	public function index()
	{
               url::redirect('/reception/page');
	 
	}

	public function page($page = 1)
          {
              $rec = new Reception_Model;
                $this->template->nav = array('/reception' => 'Приемная', '/reception/page/'.$page => 'Страница #'.$page);
                $this->template->section = 'Приемная';
                $this->template->child_view = new View('reception/page');
                $count = $rec->get_quest_count();
                $this->template->child_view->page = $page; 
	      $this->pagination = new Pagination(array(
			'uri_segment'    => 'page',
			'total_items'    => $count,
			'items_per_page' => 2,
			'style'          => 'punbb'
		));
 
          }
   public function add($page = 1)
		  {
		  /*
          $this->template->nav = array('/reception' => 'Приемная', '/reception/page/'.$page => 'Страница #'.$page);
          $this->template->section = 'Приемная';
		  $this->template->child_view = new View('reception/page');
		  $this->template->child_view->page = $page;
		  */
		   if(isset($_POST['q'],$_POST['name'],$_POST['mail'],$_POST['captcha_response']))
		     {
			   if(valid::email($_POST['mail']) && Captcha::valid($_POST['captcha_response']))
			     {
			        $rec = new Reception_Model;
		            $rec->SetQuestion($_POST['q'],$_POST['name'],$_POST['mail']);
					url::redirect('/reception/page');	 
				 }
			 }
			//else 
			url::redirect('/reception/page'); 
		  }
    public function answer($action = null)
	{
	
	switch ($action)
		{
			case 'new': 
			      if(Admin_Model::is_admin_auth())
				  {
				     if(isset($_POST['name'],$_POST['id_r']))
					  {
			            $this->template->nav = array('/reception' => 'Приемная');
                        $this->template->section = 'Ответ для '.$_POST['name'];
		                $this->template->child_view = new View('reception/answer');
						$this->template->child_view->id = $_POST['id_r'];
					  }
				   }
				   else url::redirect('/reception/page');
			    break;
			 case 'add' :
			       if(Admin_Model::is_admin_auth())
				    {
			         if(isset($_POST['q'],$_POST['id_']))
					  {
					     $a = Admin_Model::get_dep_id(Admin_Model::is_admin_auth());
						 Reception_Model::SetAnswer($_POST['q'],$_POST['id_'],$a->dep_id);
						 url::redirect('/reception/page');
					  }
					}
			        url::redirect('/reception/page');
			    break;
				case 'del' :
			       if(Admin_Model::is_admin_auth())
				    {
			         if(isset($_POST['id_r']))
					  {
						 Reception_Model::DelQuestion($_POST['id_r']);
						 url::redirect('/reception/page');
					  }
					}
			        url::redirect('/reception/page');
			    break;
			default :  url::redirect('/reception/page');
			    break;
			             		
		} 
	}		  
		  
}
?>
