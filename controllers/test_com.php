<?php defined('SYSPATH') OR die('No direct access allowed.');

class Test_Com_Controller extends Template_Controller {
	public $template = 'index';

	public function index($ID = NULL)
	{
		$this->template->nav = null;
		$this->template->section = 'Новостная лента';
		$this->template->child_view = 'основной html-код контроллера';
	}
}
?>