<?php defined('SYSPATH') OR die('No direct access allowed.');

class Map_Controller extends Template_Controller {

	public $template = 'index';

	// список созывов
	public function index()
	{
		$this->template->nav = array('/map' => 'Карта сайта');
		$this->template->section = 'Карта сайта';
		$this->template->child_view = new View('map/index');
	}
}
?>