<?php defined('SYSPATH') OR die('No direct access allowed.');

class Workgroup_Model extends Model {

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * получить все рабочие группы
	 *
	 * @return resource
	 */
	public static function get_groups()
	{
		$res = Database::instance()->get('workgroup');
		return count($res) ? $res : array();
	}

	public function get_members($id)
	{
		$res = $this->db->query('SELECT dep_id,fio FROM deput WHERE dep_id IN (SELECT dep_fk FROM group_compos WHERE gr_fk=?)', array(intval($id)));
		return count($res) ? $res : array();
	}

	public static function add_group($title)
	{
		if (!empty($title))
		{
			return count(Database::instance()->insert('workgroup', array('group_title'=>$title)));
		}
		return null;
	}

	public static function del_group($c)
	{
		if (is_array($c) && !empty($c))
		{
			foreach ($c as $id) {
				Database::instance()->delete('workgroup', array('group_id' => intval($id)));
			}
		}
	}
}