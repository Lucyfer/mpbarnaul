<?php defined('SYSPATH') OR die('No direct access allowed.');

class Conv_Model extends Model {

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * получить список созывов (упорядочен в обратном порядке)
	 *
	 * @return resource
	 */
	public static function get_convening()
	{
		/*$db = Database::instance()->orderby('conv_id', 'DESC')->get('convocation');
		$res = $db->orderby('conv_id', 'DESC')->get('convocation');*/
		$res = Database::instance()->orderby('conv_id', 'DESC')->get('convocation');
		return (count($res) > 0) ? $res : array();
	}

	/**
	 * состав созыва
	 *
	 * @param int $id первичный ключ созыва
	 * @return resource
	 */
	public function get_staff($id)
	{
		$res = Database::instance()->query('SELECT dep_id,fio FROM deput WHERE dep_id IN (SELECT dep_fk FROM compos WHERE conv_fk=?)', array(intval($id)));
		return (count($res) > 0) ? $res : array();
	}

	/**
	 * состав группы
	 *
	 * @param int $id
	 * @return res
	 */
	public static function get_staff_group($id)
	{
		$res = Database::instance()->query('SELECT dep_id,fio FROM deput WHERE dep_id IN (SELECT dep_fk FROM group_compos WHERE gr_fk=?)', array(intval($id)));
		return (count($res) > 0) ? $res : array();
	}

	/**
	 * информация о депутате
	 *
	 * @param int $id
	 * @return resource
	 */
	public function get_deputy_info($id)
	{
		$res = Database::instance()->getwhere('deput', array('dep_id' => intval($id)));
		return (count($res) > 0) ? $res->result()->current() : array();
	}

	/**
	 * добавить новый созыв
	 *
	 * @param string $title
	 * @return int
	 */
	public static function add_conv($title)
	{
		if (!empty($title))
		{
			return count(Database::instance()->insert('convocation', array('conv_text'=>$title)));
		}
		return null;
	}

	/**
	 * исключить депутата из созыва
	 *
	 * @param int $conv
	 * @param array $list
	 */
	public static function del_from_conv($conv, $list)
	{
		$conv = intval($conv);
		if (is_array($list) && !empty($list) && $conv > 0)
		{
			foreach ($list as $id)
			{
				Database::instance()->delete('compos', array('conv_fk' => $conv, 'dep_fk' => intval($id)));
			}
		}
	}

	/**
	 * удалить из группы
	 *
	 * @param int $gr группа
	 * @param array $list список депутатов
	 */
	public static function del_from_group($gr, $list)
	{
		$gr = intval($gr);
		if (is_array($list) && !empty($list) && $gr > 0)
		{
			foreach ($list as $id)
				Database::instance()->delete('group_compos', array('gr_fk' => $gr, 'dep_fk' => intval($id)));
		}
	}

	/**
	 * добавить депутата к созыву
	 *
	 * @param int $conv
	 * @param array $dep
	 */
	public static function add_deput_to_conv($conv, $dep)
	{
		$conv = intval($conv);
		if (is_array($dep) && !empty($dep) && $conv > 0)
		{
			foreach ($dep as $id)
			{
				Database::instance()->insert('compos', array('conv_fk' => $conv, 'dep_fk' => intval($id)));
			}
		}
	}

	/**
	 * добавить депутата к группе
	 *
	 * @param int $gr
	 * @param array $dep
	 */
	public static function add_deput_to_group($gr, $dep)
	{
		$gr = intval($gr);
		if (is_array($dep) && !empty($dep) && $gr > 0)
		{
			foreach ($dep as $id)
			{
				Database::instance()->insert('group_compos', array('gr_fk' => $gr, 'dep_fk' => intval($id)));
			}
		}
	}

	/**
	 * депутаты не входящие в созыв
	 *
	 * @param int $conv
	 * @return resource
	 */
	public static function deputies_not_in_conv($conv)
	{
		$res = Database::instance()->query('SELECT dep_id,fio FROM deput WHERE dep_id NOT IN (SELECT dep_fk FROM compos WHERE conv_fk=?)', array(intval($conv)));
		return count($res) ? $res : array();
	}

	/**
	 *  депутаты не входящие в группу
	 *
	 * @param int $id
	 * @return res
	 */
	public static function deputies_not_in_group($id)
	{
		$res = Database::instance()->query('SELECT dep_id,fio FROM deput WHERE dep_id NOT IN (SELECT dep_fk FROM group_compos WHERE gr_fk=?)', array(intval($id)));
		return count($res) ? $res : array();
	}

	/**
	 * получить список всех депутатов
	 *
	 * @return res
	 */
	public static function get_all_deputies()
	{
		$res = Database::instance()->query('SELECT dep_id,fio FROM deput ORDER BY fio');
		return count($res) ? $res : array();
	}

	/**
	 * новый депутат
	 *
	 * @param array $info
	 * @return int
	 */
	public static function add_new_deputy($info)
	{
		if (is_array($info) && sizeof($info)==3)
		{
			foreach ($info as $i)
				if (empty($i))
					return null;
			return count(Database::instance()->insert('deput', array('fio'=>$info[0], 'phone'=>$info[1], 'mail'=>$info[2])));
		}
	}

	public static function update_dep($info)
	{
		if (is_array($info) && sizeof($info)==4)
		{
			foreach ($info as $i=>$j)
				if (empty($j))
					return null;
			return count(Database::instance()
				->update('deput',
					array('fio'=>$info['fio'],'phone'=>$info['phone'],'mail'=>$info['mail']),
					array('dep_id'=>intval($info['id']))));
		}
	}
	
	public static function drop_dep($arr_dep)
	{
		if (is_array($arr_dep))
		{
			foreach ($arr_dep as $i){
				if (empty($i))
					return null;
				  Database::instance()->query('delete from deput where dep_id='.intval($i));
				}
		}
	}

	/**
	 * удалить созывы
	 *
	 * @param array $c первичные ключи созывов
	 */
	public static function del_conv($c)
	{
		if (is_array($c) && !empty($c))
		{
			foreach ($c as $id) {
				Database::instance()->delete('convocation', array('conv_id' => intval($id)));
			}
		}
	}
}
?>