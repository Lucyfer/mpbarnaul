<?php defined('SYSPATH') OR die('No direct access allowed.');

class News_Model extends Model {

	public function __construct()
	{
		parent::__construct();
	}

	const TITLE = 0;
	const DATE  = 1;

	/**
	 * формат даты
	 *
	 * @param date $d
	 * @return date
	 */
	public function format_date($d)
	{
		return date('[d.m.Y]', strtotime($d));
	}

	/**
	 * получить новости из бд
	 *
	 * @param int $count количество
	 * @param int $offset смещение
	 * @return resource
	 */
	public function get_news($count=15, $offset=0)
	{
		$res = Database::instance()->orderby('news_id', 'DESC')->limit($count, $offset)->get('news');
		if(count($res) > 0)
		 	return $res;
		 else
		 	return array();
	}

	/**
	 * выдать все новости
	 *
	 * @return res
	 */
	public function get_all()
	{
		$res = Database::instance()->select(array('news_id','news_date','news_title'))->orderby('news_id', 'DESC')->get('news');
		return count($res) ? $res : array();
	}

	/**
	 * общее количество новостей
	 *
	 * @return int
	 */
	public function get_news_count()
	{
		return Database::instance()->count_records('news');
	}

	/**
	 * поиск по новостям
	 *
	 * @param string $str
	 * @param const int $where
	 * @return resource
	 */
	public function search($str, $where)
	{
		$res = null;
		switch ($where)
		{
			case News_Model::TITLE:
				$res = $this->db->like('news_title', $str)->get('news');
				break;
			case News_Model::DATE:
				if (preg_match('/^\d{4}-\d{2}-\d{2}$/', $str))
					$res = $this->db->getwhere('news', array('news_date' => $str));
				break;
			default: return null;
		}
		if (count($res) > 0)
		 	return $res;
		 else
		 	return array();
	}

	/**
	 * добавить новость
	 *
	 * @param string $title название
	 * @param string $desc краткое описание
	 * @param string $text полное описание
	 */
	public function add_news($info)
	{
		if (is_array($info) && sizeof($info)==3)
		{
			foreach($info as $k=>&$v)
			{
				$v = trim($v);
				if (empty($v))
					return null;
			}
			return count(Database::instance()->query('INSERT INTO news(news_title,news_desc,news_text) VALUES(?, ?, ?)',
				array(
					News_Model::html_entities($info['title']),
					News_Model::html_entities($info['desc']),
					$info['text'])));
		}
		return null;
	}

	/**
	 * обновление новости
	 *
	 */
	public function update_news($info)
	{
		if (is_array($info) && sizeof($info)==4)
		{
			foreach($info as $k=>&$v)
			{
				$v = trim($v);
				//$v = preg_replace('/^\s+/','',$v);
				//$v = preg_replace('/\s+$/','',$v);
				if (empty($v))
					return null;
			}

			return count(Database::instance()
				->update('news',
					array(
						'news_title'=>News_Model::html_entities($info['title']),
						'news_desc' =>News_Model::html_entities($info['desc']),
						'news_text' =>$info['text']),
					array('news_id' =>intval($info['id']))));
		}
		return null;
	}

	/**
	 * костыль нах, ибо гениальный пых не осилил utf8
	 *
	 * @param unknown_type $str
	 * @return unknown
	 */
	public function html_entities($str)
	{
		return	preg_replace("/'/",'&#39;',
				preg_replace('/</','&lt;',
				preg_replace('/>/','&gt;',
				preg_replace('/&/','&amp;',$str))));
	}

	/**
	 * удалить новости
	 *
	 * @param array $ids
	 * @return int
	 */
	public function delete_news($ids)
	{
		//return count(Database::instance()->delete('news', array('news_id' => intval($ids))));
		if (is_array($ids) && !empty($ids))
		{
			foreach ($ids as $id)
			{
				Database::instance()->delete('news', array('news_id'=>intval($id)));
			}
		}
	}

	/**
	 * дата последней новости
	 *
	 * @return date
	 */
	public function get_last_date()
	{
		$res = Database::instance()->query('SELECT news_date FROM news ORDER BY news_id DESC LIMIT 1');
		return count($res) ? $res->result()->current()->news_date : NULL;
	}

	/**
	 * данные о новости
	 *
	 * @param int $id
	 * @return resource
	 */
	public function get_news_data($id)
	{
		$res = Database::instance()->where(array('news_id' => intval($id)))->get('news');
		return count($res) ? $res->result()->current() : array();
	}
}
?>