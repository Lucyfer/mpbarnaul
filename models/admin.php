<?php defined('SYSPATH') OR die('No direct access allowed.');

class Admin_Model extends Model {

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * используемый хэш
	 *
	 * @param string $h строка от которой нужен хэш
	 * @return string
	 */
	public static function hash_func($h)
	{
		return sha1($h);
	}

	/**
	 * проверка наличия сессии админа
	 * возвращает первичный ключ из таблицы админов или 0
	 * @return int
	 */
	 
	public static function is_admin_auth()
	{
		return intval(Session::instance()->get('admin'));
	}

	/**
	 * получить логин админа
	 *
	 * @param int $pk
	 * @return res
	 */
	public static function get_admin_login($pk)
	{
		$db = Database::instance()
			->select('admin_login')
			->where(array('admin_id'=>intval($pk)))
			->get('admins');
		return count($db) ? $db->result()->current() : null;
	}
	public static function get_dep_id($pk)
	{
		$db = Database::instance()
			->select('dep_id')
			->where(array('admin_id'=>intval($pk)))
			->get('admins');
		return count($db) ? $db->result()->current() : null;
	}

	public function logout()
	{
		Session::instance()->destroy();
	}

	/**
	 * стартует сессию
	 *
	 * @param int $id первичный ключ админа
	 */
	public static function set_admin_auth($id)
	{
		Session::instance()->set('admin', $id);
	}

	/**
	 * проверка логина и пароля, возвращает объект из бд
	 *
	 * @param string $login
	 * @param string $pass
	 * @return int
	 */
	public static function check_admin_auth($login, $pass)
	{
		$db = new Database();
		$res = $db->query('SELECT * FROM admins WHERE admin_login=? AND admin_passw=?',
			array($login, Admin_Model::hash_func($pass)));

		if(count($res) > 0)
			return $res->result()->current();
		else
			return FALSE;
	}
	
	public static function check_write_news($log)
	   {
	     $db = new Database();
		 $res = $db->query('SELECT p_news_w FROM admins WHERE admin_login=?',
			array($log));
			if(count($res) > 0)
			 {
			   $a = $res->result()->current();
			     if(intval($a->p_news_w) === 1)
				   return TRUE;
				 else 
				   return FALSE;  
			 }
		    else
			  return FALSE;  
	   }
	 public static function check_write_convocations($log)
	   {
	     $db = new Database();
		 $res = $db->query('SELECT p_conv_w FROM admins WHERE admin_login=?',
			array($log));
			if(count($res) > 0)
			 {
			   $a = $res->result()->current();
			     if(intval($a->p_conv_w) === 1)
				   return true;
				 else 
				   return false;  
			 }
		    else
			  return FALSE;  
	   }
	 public static function check_answer_reception($log)
	   {
	     $db = new Database();
		 $res = $db->query('SELECT p_rec_answ FROM admins WHERE admin_login=?',
			array($log));
			if(count($res) > 0)
			 {
			   $a = $res->result()->current();
			     if(intval($a->p_rec_answ) === 1)
				   return true;
				 else 
				   return false;  
			 }
		    else
			  return FALSE;  
	   }  
	 public static function check_write_workgroup($log)
	   {
	     $db = new Database();
		 $res = $db->query('SELECT p_wg_w FROM admins WHERE admin_login=?',
			array($log));
			if(count($res) > 0)
			 {
			   $a = $res->result()->current();
			     if(intval($a->p_wg_w) === 1)
				   return true;
				 else 
				   return false;  
			 }
		    else
			  return FALSE;  
	   } 
	   public static function check_edit_deput($log)
	   {
	     $db = new Database();
		 $res = $db->query('SELECT p_dep_w FROM admins WHERE admin_login=?',
			array($log));
			if(count($res) > 0)
			 {
			   $a = $res->result()->current();
			     if(intval($a->p_dep_w) === 1)
				   return true;
				 else 
				   return false;  
			 }
		    else
			  return FALSE;  
	   }   
	   public static function check_write_archiv($log)
	   {
	     $db = new Database();
		 $res = $db->query('SELECT p_arch_w FROM admins WHERE admin_login=?',
			array($log));
			if(count($res) > 0)
			 {
			   $a = $res->result()->current();
			     if(intval($a->p_arch_w) === 1)
				   return true;
				 else 
				   return false;  
			 }
		    else
			  return FALSE;  
	   }
	   public static function check_write_video($log)
	   {
	     $db = new Database();
		 $res = $db->query('SELECT p_video FROM admins WHERE admin_login=?',
			array($log));
			if(count($res) > 0)
			 {
			   $a = $res->result()->current();
			     if(intval($a->p_video) === 1)
				   return true;
				 else 
				   return false;  
			 }
		    else
			  return FALSE;  
	   }
	   public static function check_write_info($log)
	   {
	     $db = new Database();
		 $res = $db->query('SELECT p_about FROM admins WHERE admin_login=?',
			array($log));
			if(count($res) > 0)
			 {
			   $a = $res->result()->current();
			     if(intval($a->p_about) === 1)
				   return true;
				 else 
				   return false;  
			 }
		    else
			  return FALSE;  
	   }   
}