<?php defined('SYSPATH') OR die('No direct access allowed.');
   class Reception_Model extends Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function GetQuestions($count = 10 , $offset = 0)
	 {
	  
                   $res = $this->db->orderby('rec_id', 'DESC')->limit($count, $offset)->get('reception');
		if(count($res) > 0)
		 	return $res;
		 else
		 	return FALSE;

	 }
        public function GetQuestions_by_name($name = null,$count = 10 , $offset = 0)
        {
             // добавить рег экспы на проверку имени
            
                 $res = $this->db->where(array('rec_name' => $name))->limit($count, $offset)->get('reception');
		if(count($res) > 0)
		 	return $res;
		 else
		 	return null;
        }
        public function GetAnswers_by_name($name = null)
        {
              // добавить рег экспы на проверку имени
            
           $res = $this->db->query("select * from reception where dep_fk in (select dep_id where fio like '$name%' from deput)");
                          
		if(count($res) > 0)
		 	return $res;
		 else
		 	return null;
        }
        public function SetQuestion($quest = null,$name = null,$email = null)
        {
            $this->db->query('INSERT INTO reception VALUES(default, ?, ?, ?,NULL, NULL)',
			array($name, $email, $quest));
        }
        public function SetAnswer($answ = null,$id = 0,$id_dep = 0)
        {
            Database::instance()->query("update reception set rec_answer='".$answ."',dep_fk=".$id_dep." where rec_id=".$id);
        }
        public function get_quest_count()
	    {
		return $this->db->count_records('reception');
	     }
       public function GetDep($id = 0)
         {
              return Database::instance()->where(array('dep_id' => $id))->limit(1,0)->get('deput');
         }
	   public function DelQuestion($id =0)
	     {
		    Database::instance()->query("delete from reception where rec_id=".intval($id));
		 }	 
	   public function Check_Answer($id = 0)
	     {
		     $res =  Database::instance()->query("select dep_fk from reception where rec_id=".$id);
			 if(count($res) > 0)
			     {
		 	       $a = $res->result()->current();
				     if($a->dep_fk != NULL)
					    return true;
					 else
					    return false;	
				 }
		 else
			return false;
		 }	 
  }
?>