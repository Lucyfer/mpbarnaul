<?php defined('SYSPATH') OR die('No direct access allowed.');

class Arch_Model extends Model {

	public function __construct()
	{
		parent::__construct();
	}

	const DATE = 0;
	const TITLE = 1;
	const BODY = 2;

	/**
	 * получить документы (дата название) из базы
	 *
	 * @param int $count
	 * @param int $offset
	 * @return resource
	 */
	public function get_docs($count=50, $offset=0)
	{
		$res = Database::instance()
			->select(array('doc_id', 'doc_date', 'doc_title'))
			->orderby('doc_id', 'DESC')
			->limit($count, $offset)
			->get('docs');
		return (count($res) > 0) ? $res : array();
	}

	/**
	 * список заголовков всех документов
	 *
	 * @return res
	 */
	public static function get_all_docs()
	{
		$res = Database::instance()
			->select(array('doc_id','doc_title'))
			->orderby('doc_id', 'DESC')
		//	->limit($count, $offset)
			->get('docs');
		return (count($res) > 0) ? $res : array();
	}

	public function del_doc($docs)
	{
		if (is_array($docs) && !empty($docs))
		{
			foreach ($docs as $id) {
				Database::instance()->delete('docs', array('doc_id' => intval($id)));
			}
		}
		else return null;
	}

	/**
	 * получить документ по id
	 *
	 * @param int $id
	 * @return resource
	 */
	public function get_doc_info($id)
	{
		$res = Database::instance()->getwhere('docs', array('doc_id' => intval($id)));
		return (count($res) > 0) ? $res->result()->current() : array();
	}

	/**
	 * добавить документ
	 *
	 * @param string $data заголовок | тело документа
	 * @return int
	 */
	public function add_doc(/*$title, $text*/ $data)
	{
		if (is_array($data) && sizeof($data)==2)
		{
			foreach ($data as $d) if (empty($d)) return null;
			return count(Database::instance()->insert('docs',
				array(
				'doc_title' => strip_tags($data[0]),
				'doc_text'  => $data[1])));
		}
		else
			return null;
	}

	/**
	 * обновить документ
	 *
	 * @param int $id
	 * @param array $data
	 * @return int
	 */
	public static function update_doc($id, $data)
	{
		if (is_array($data) && sizeof($data)==2)
			return count(Database::instance()->update('docs',
				array(
				'doc_title' => strip_tags($data[0]),
				'doc_text'  => $data[1]
				),
				array('doc_id'=>intval($id))));
		else
			return null;
	}

	/**
	 * количество документов
	 *
	 * @return int
	 */
	public function get_docs_count()
	{
		return Database::instance()->count_records('docs');
	}

	/**
	 * поиск в бд
	 *
	 * @param string $str
	 * @param const int $where
	 */
	public function search_docs($str, $where)
	{
		$res = null;
		switch ($where)
		{
			case Arch_Model::TITLE:
				$res = $this->db->like('doc_title', $str)->get('docs');
				break;

			case Arch_Model::DATE:
				if (preg_match('/^\d{4}-\d{2}-\d{2}$/', $str))
					$res = $this->db->getwhere('docs', array('doc_date' => $str));
				else echo 'неправильный формат даты (требуется год-месяц-день YYYY-MM-DD)';
				break;

			case Arch_Model::BODY:
				$res = $this->db->like('doc_text', $str)->get('docs');
				break;

			default:
				return array();
				break;
		}
		return (count($res) > 0) ? $res : array();
	}
}